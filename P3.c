#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_BUFFER 1024
#define SEPARATORS " \t\n"
#define MAX_ARGS 64
extern char **environ;

void main() {
 char c[MAX_BUFFER];
 char myclear[12] = "myclear";
 char mydir[12] = "mydir";
 char myenv[12] = "myenviron";
 char myquit[12] = "myquit";
 char cal[12] = "cal";
 int x = 0;

//A simple loop to simulate the shell
 while(x == 0){
   //Each loop begins with a prompt for a command
   printf("Enter Command: ");
   //We use the fgets function to include spacing in the input string
   fgets(c, MAX_BUFFER, stdin);
   char* args[MAX_ARGS];
   int i;
   //Here we are invoking a tokenization of the input by separation of spaces
   args[0] = strtok(c, SEPARATORS);

   /*
      Each loop will transverse a set of conditionals which are determined
      by the first argument(word)
   */
   if(strcmp(myclear, args[0]) == 0){
      system("clear");
   }
   //Here we have a two-argument command
   else if(strcmp(mydir, args[0]) == 0){
      /*
        If the user initiates the mydir command then a second
        argument will be scanned for to provide a directory name
      */
      for (i = 0; args[i] = strtok(NULL, SEPARATORS); i++){
      }
      /*
        In order to construct a system compatible command, we have to
        first copy the command pre-fix into a designated string
      */
      char runString[MAX_BUFFER];
      char* preFix = "ls -al ";
      /*
        After the command pre-fix, we add a null-terminating character
        to signify the end of that character array
      */
      runString[7] = '\0';
      strncpy(runString, preFix, sizeof(preFix));
      //Then we concatenate the pre-fix and specified directory token
      strncat(runString, args[0], (sizeof(args[0])*2));
      //Lastly we run the command string
      system(runString);

   }
   else if(strcmp(myenv, args[0]) == 0){
      char ** env = environ;
      while(*env)
         printf("%s\n",*env++);
   }
   else if (strcmp(myquit, args[0]) == 0){
      system("exit");
      x++;
   }
   else if(strcmp(cal, args[0]) == 0){
      system("cal");
   }
   /*
     If the command does not match any of our developed commands
     then we just run the command directly to the system
   */
   else {
      system(args[0]);
   }
  }
 }
